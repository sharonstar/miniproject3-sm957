// Use CDK to create a S3 bucket with versioning and encryption enabled.
import { App, Stack, StackProps } from 'aws-cdk-lib';
import { Bucket } from 'aws-cdk-lib/aws-s3';
import { aws_s3 as s3 } from 'aws-cdk-lib';

export class Week3Stack extends Stack {
  constructor(scope: App, id: string, props?: StackProps) {
    super(scope, id, props);

    new Bucket(this, 'week3-bucket', {
      versioned: true,
      encryption: s3.BucketEncryption.KMS_MANAGED
    });
  }
}



