# Week3 Project

Requirements: Create an S3 Bucket using CDK with AWS CodeWhisperer. 
* Create S3 bucket using AWS CDK
* Use CodeWhisperer to generate CDK code
* Add bucket properties like versioning and encryption

# Steps

1. Install relative packages and tools: 

install AWS Toolkit in VScode

install Node.js and AWS CDK    `npm install -g aws-cdk`

install the S3 Package  `npm install @aws-cdk/aws-s3`

2. Initialize your CDK project

`cdk init app --language=typescript`


3. Use CodeWhisperer to generate CDK code, and add bucket properties like versioning and encryption

write comment to add versioning and encryption, and CodeWhisperer will generate matching code automatically.

![](img3.png)

4. Creates an Amazon S3 Bucket  `cdk bootstrap`

![](img4.png)

In the Amazon S3, you can see a new bucket has been created.

![](img1.png)

5. Deploy the project   `cdk deploy`

![](img5.png)

click the bucket, you can see the object.

![](img2.png)





